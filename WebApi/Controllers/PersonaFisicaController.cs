﻿using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;
using WebApi.Models;


namespace tester.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonaFisicaController : ControllerBase
    {
        private readonly ExamenContext context;
        private readonly IConfiguration configuration;
        private readonly string connectionString;

        public PersonaFisicaController(ExamenContext context,IConfiguration configuration )
        {
            this.context = context;
            this.configuration = configuration;
            connectionString = configuration.GetConnectionString("DefaultConnection");
        }
        [HttpGet("{rfc}")]
        public async Task<ActionResult<PersonaFisica>> Get(string rfc)
        {
            var existe = await context.PersonaFisica.AnyAsync(x => x.Rfc == rfc && x.Activo == true);
            if (!existe)
            {
                return NotFound();
            }
            using var connection = new SqlConnection(connectionString);

           var persona =  await connection.QuerySingleAsync<PersonaFisica>(@"SELECT * FROM [dbo].[Tb_PersonasFisicas] where Rfc = @rfc AND activo = 1 ", new { rfc });
            return Ok(persona);
        }

        [HttpGet]
        [Route("/api/PersonasFisicas")]
        public async Task<ActionResult<List<PersonaFisica>>> GetAll()
        {
            using var connection = new SqlConnection(connectionString);
            var personas = await connection.QueryAsync<PersonaFisica>(@"SELECT * FROM [dbo].[Tb_PersonasFisicas] WHERE activo = 1");
            return Ok(personas);
        }
        [HttpPost]
        public ActionResult Post(PersonaFisica personaFisica)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            using var connection = new SqlConnection(connectionString);
            var procedure = "[dbo].[sp_AgregarPersonaFisica]";
            var values = new { Nombre = personaFisica.Nombre, apellidoPaterno = personaFisica.ApellidoPaterno,apellidoMaterno = personaFisica.ApellidoMaterno,rfc = personaFisica.Rfc,fechanacimiento=personaFisica.FechaNacimiento,usuarioagrega=personaFisica.UsuarioAgrega};
            var results = connection.Query(procedure, values, commandType: CommandType.StoredProcedure).FirstOrDefault();

            
            int err =(int)results.ERROR;
            var msj = results.MENSAJEERROR;
            if(err <= 0)
            {
                return BadRequest(msj);
            }
  
            return Ok();
        }
        [HttpPut("{id:int}")]// api/autores/1
        public async Task<ActionResult<PersonaFisica>> Put(PersonaFisica personaFisica,int id)
        {

            var existe = await context.PersonaFisica.AnyAsync(x => x.IdPersonaFisica == id && x.Activo == true);
            if (!existe)
            {
                return NotFound();
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            using var connection = new SqlConnection(connectionString);
            var procedure = "[dbo].[sp_ActualizarPersonaFisica]";
            var values = new { IdPersonaFisica= id, Nombre = personaFisica.Nombre, apellidoPaterno = personaFisica.ApellidoPaterno, apellidoMaterno = personaFisica.ApellidoMaterno, rfc = personaFisica.Rfc, fechanacimiento = personaFisica.FechaNacimiento, usuarioagrega = personaFisica.UsuarioAgrega };
            var results = connection.Query(procedure, values, commandType: CommandType.StoredProcedure).FirstOrDefault();

            int err = (int)results.ERROR;
            var msj = results.MENSAJEERROR;
            if (err <= 0)
            {
                return BadRequest(msj);
            }

            return Ok();
        }
        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var existe = await context.PersonaFisica.AnyAsync(x => x.IdPersonaFisica == id && x.Activo == true);
            if (!existe)
            {
                return NotFound("No existe esa Persona Fisica");
            }

            using var connection = new SqlConnection(connectionString);
            var procedure = "[dbo].[sp_EliminarPersonaFisica]";
            var values = new { IdPersonaFisica = id};
            connection.Query(procedure, values, commandType: CommandType.StoredProcedure);
            return Ok();
        }

    }
}
