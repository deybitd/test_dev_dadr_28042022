﻿using FrontEnd.Servicios;
using Microsoft.AspNetCore.Mvc;
using FrontEnd.Models;
using Microsoft.AspNetCore.Identity;

namespace FrontEnd.Controllers
{
    public class PersonasFisicasController : Controller
    {
        private readonly IRepositorioPersonasFisicas repositorioPersonasFisicas;
        private readonly HttpContext httpContext;
        private readonly int _RegistrosPorPagina = 20;
        private PaginadorGenerico<PersonaFisica> _Paginador;
        public PersonasFisicasController(IRepositorioPersonasFisicas repositorioPersonasFisicas, IHttpContextAccessor httpContextAccessor)
        {
            this.repositorioPersonasFisicas = repositorioPersonasFisicas;
            httpContext = httpContextAccessor.HttpContext;
        }
        public async Task<IActionResult> Index(int pagina = 1)
        {


            var personasAll = await repositorioPersonasFisicas.GetPersonasFisicas();
            int _TotalRegistros = personasAll.Count;
            var _TotalPaginas = (int)Math.Ceiling((double)_TotalRegistros / _RegistrosPorPagina);
           var personas = personasAll.OrderBy(x => x.IdPersonaFisica).Skip((pagina - 1) * _RegistrosPorPagina).Take(_RegistrosPorPagina).ToList();
            _Paginador = new PaginadorGenerico<PersonaFisica>()
            {
                RegistrosPorPagina = _RegistrosPorPagina,
                TotalRegistros = _TotalRegistros,
                TotalPaginas = _TotalPaginas,
                PaginaActual = pagina,
                Resultado = personas
            };
            //PersonaFisica persona = new PersonaFisica() { IdPersonaFisica=4, Activo = true,ApellidoMaterno = "MVc",Rfc="DERD970403E94",Nombre ="DAvid",ApellidoPaterno="delgado",UsuarioAgrega=2};
            //var result = repositorioPersonasFisicas.DeletePersonaFisica(4);
            return View(_Paginador);
        }
        public async Task<IActionResult> Crear()
        {
            var personas = await repositorioPersonasFisicas.GetPersonasFisicas();

            //PersonaFisica persona = new PersonaFisica() { IdPersonaFisica=4, Activo = true,ApellidoMaterno = "MVc",Rfc="DERD970403E94",Nombre ="DAvid",ApellidoPaterno="delgado",UsuarioAgrega=2};
            //var result = repositorioPersonasFisicas.DeletePersonaFisica(4);
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Crear(PersonaFisica persona)
        {
            persona.FechaRegistro = DateTime.Today;

            var userId = httpContext.User.Claims.First().Value;
            persona.UsuarioAgrega = int.Parse(userId);
            await repositorioPersonasFisicas.PostPersonaFisica(persona);

            //PersonaFisica persona = new PersonaFisica() { IdPersonaFisica=4, Activo = true,ApellidoMaterno = "MVc",Rfc="DERD970403E94",Nombre ="DAvid",ApellidoPaterno="delgado",UsuarioAgrega=2};
            //var result = repositorioPersonasFisicas.DeletePersonaFisica(4);
            return RedirectToAction("Index");
        }
        
        public async Task<IActionResult> Eliminar(int id)
        {
            
            await repositorioPersonasFisicas.DeletePersonaFisica(id);

           
            return RedirectToAction("Index");
        }

        public IActionResult Editar(int id)
        {
            
            ViewBag.Id = id;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> PutPersonaFisica(PersonaFisica persona,int id)
        {
            await repositorioPersonasFisicas.PutPersonaFisica(id, persona);
            return RedirectToAction("index");
        }

        public IActionResult Consultar()
        {
            
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Mostrar(PersonaFisica persona)
        {
           var result =  await repositorioPersonasFisicas.GetPersonaFisica(persona.Rfc);
         
            return View(result);
        }
    }
}
