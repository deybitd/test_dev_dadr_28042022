﻿using ClosedXML.Excel;
using FrontEnd.Models;
using FrontEnd.Servicios;
using Microsoft.AspNetCore.Mvc;

namespace FrontEnd.Controllers
{
    public class ReportesController : Controller
    {
        private readonly IRepositorioReportes repositorioReportes;
        private readonly int _RegistrosPorPagina = 20;
        private PaginadorGenerico<Reportes> _Paginador;
        public ReportesController(IRepositorioReportes repositorioReportes)
        {
            this.repositorioReportes = repositorioReportes;
        }
        public async Task<IActionResult> Index(int pagina = 1)
        {
            var result = await repositorioReportes.GetReportesToken();
            var reportesAll = await repositorioReportes.GetReportes(result.Data);
            int _TotalRegistros = reportesAll.Count;
            var reportes = reportesAll.OrderBy(x => x.IdCliente).Skip((pagina - 1) * _RegistrosPorPagina).Take(_RegistrosPorPagina).ToList();
            var _TotalPaginas = (int)Math.Ceiling((double)_TotalRegistros / _RegistrosPorPagina);
            _Paginador = new PaginadorGenerico<Reportes>()
            {
                RegistrosPorPagina = _RegistrosPorPagina,
                TotalRegistros = _TotalRegistros,
                TotalPaginas = _TotalPaginas,
                PaginaActual = pagina,
                Resultado = reportes
            };
            return View(_Paginador);
        }
        
        public async Task<IActionResult> Excel()
        {
            var result = await repositorioReportes.GetReportesToken();
            var reportesAll = await repositorioReportes.GetReportes(result.Data);
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Reportes");
                var currentRow = 1;
                worksheet.Cell(currentRow, 1).Value = "Id Cliente";
                worksheet.Cell(currentRow, 2).Value = "Fecha de Registro";
                worksheet.Cell(currentRow, 3).Value = "Razon Social";
                worksheet.Cell(currentRow, 4).Value = "RFC";
                worksheet.Cell(currentRow, 5).Value = "Sucursal";
                worksheet.Cell(currentRow, 6).Value = "Id Empleado";
                worksheet.Cell(currentRow, 7).Value = "Nombre";
                worksheet.Cell(currentRow, 8).Value = "Apellido Paterno";
                worksheet.Cell(currentRow, 9).Value = "Apellido Materno";
                worksheet.Cell(currentRow, 10).Value = "Id Viaje";
                foreach (var reporte in reportesAll)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = reporte.IdCliente;
                    worksheet.Cell(currentRow, 2).Value = reporte.FechaRegistroEmpresa.ToString();
                    worksheet.Cell(currentRow, 3).Value = reporte.RazonSocial;
                    worksheet.Cell(currentRow, 4).Value = reporte.RFC;
                    worksheet.Cell(currentRow, 5).Value = reporte.Sucursal;
                    worksheet.Cell(currentRow, 6).Value = reporte.IdEmpleado;
                    worksheet.Cell(currentRow, 7).Value = reporte.Nombre;
                    worksheet.Cell(currentRow, 8).Value = reporte.Paterno;
                    worksheet.Cell(currentRow, 9).Value = reporte.Materno;
                    worksheet.Cell(currentRow, 10).Value = reporte.IdViaje;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "Reportes.xlsx");
                }
            }
        }
    }
}
