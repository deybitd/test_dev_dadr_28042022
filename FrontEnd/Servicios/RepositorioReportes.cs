﻿using FrontEnd.Models;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http.Headers;
using System.Text;

namespace FrontEnd.Servicios
{
    public interface IRepositorioReportes
    {
        Task<List<Reportes>> GetReportes(string token);
        Task<Token> GetReportesToken();
    }
    public class RepositorioReportes : IRepositorioReportes
    {
        private HttpClient httpClient;
        private readonly IConfiguration configuration;

        public RepositorioReportes(HttpClient httpClient, IConfiguration configuration)
        {
            this.httpClient = httpClient;
            this.configuration = configuration;
        }

        public async Task<Token> GetReportesToken()
        {
            httpClient = new HttpClient();
            Token result = null;
            var url = configuration.GetSection("EndPointsToka").GetValue<string>("Token");
            var json = JsonConvert.SerializeObject(new
            {
                Username = configuration.GetSection("connectionStrings").GetValue<string>("UsernameToka")
                ,
                Password = configuration.GetSection("connectionStrings").GetValue<string>("PasswordToka")
            });
            var body = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await httpClient.PostAsync(url, body);


            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                
                result = JsonConvert.DeserializeObject<Token>(content);
            }
            else
            {
                return null;
            }

            return result;
        }

        public async Task<List<Reportes>> GetReportes(string token)
        {
            string jwt = token;
            httpClient = new HttpClient();
            List<Reportes> result = null;
            var url = configuration.GetSection("EndPointsToka").GetValue<string>("Reportes");
          

            var httpRequestMessage = new HttpRequestMessage(
             HttpMethod.Get,url)
            {
                Headers =
            {
                { HeaderNames.Accept, "application/json" },
                { HeaderNames.Authorization,"Bearer "+token }
               
            }
            };
            var httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);
            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var json = await httpResponseMessage.Content.ReadAsStringAsync();

               var dat =   JsonConvert.DeserializeObject<DataReportes>(json);
                result = new List<Reportes>();
               foreach(var item in dat.Data)
                {
                    result.Add(item);
                }

            }

            return result;
        }





    }


        
}   
    

