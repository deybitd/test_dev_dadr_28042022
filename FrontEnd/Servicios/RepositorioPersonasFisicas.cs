﻿using System.Net.Http.Headers;
using System.Text.Json;
using System.Text;
using FrontEnd.Models;
using System.Net;
using Newtonsoft.Json;

namespace FrontEnd.Servicios
{
    public interface IRepositorioPersonasFisicas
    {
        Task<string> DeletePersonaFisica(int id);
        Task<PersonaFisica> GetPersonaFisica(string rfc);
        Task<List<PersonaFisica>> GetPersonasFisicas();
        Task<string> PostPersonaFisica(PersonaFisica personaFisica);
        Task<string> PutPersonaFisica(int id, PersonaFisica personaFisica);
    }
    public class RepositorioPersonasFisicas : IRepositorioPersonasFisicas
    {
        private  HttpClient httpClient;
        private readonly IConfiguration configuration;

        public RepositorioPersonasFisicas(HttpClient httpClient , IConfiguration configuration)
        {
            this.httpClient = httpClient;
            this.configuration = configuration;
        }
        public async Task<List<PersonaFisica>> GetPersonasFisicas()
        {   
            httpClient = new HttpClient();
            List<PersonaFisica> result = null;
            var url = configuration.GetSection("EndPoints").GetValue<string>("GetAll");
            using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url))
            {
              using(HttpResponseMessage response = await httpClient.SendAsync(request))
                {
                    using(HttpContent content = response.Content)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var json = await content.ReadAsStringAsync();
                             result = JsonConvert.DeserializeObject<List<PersonaFisica>>(json); 
                        }
                    }
                }
            }
            return result;
        }
        public async Task<PersonaFisica> GetPersonaFisica(string rfc)
        {
            httpClient = new HttpClient();
            PersonaFisica result = null;
            var url = configuration.GetSection("EndPoints").GetValue<string>("Api");
            url = url + "/" + rfc;
            using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url))
            {
                using (HttpResponseMessage response = await httpClient.SendAsync(request))
                {
                    using (HttpContent content = response.Content)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var json = await content.ReadAsStringAsync();
                            result = JsonConvert.DeserializeObject<PersonaFisica>(json);
                        }
                    }
                }
            }
            return result;
        }
        public async Task<PersonaFisica> GetPersonaFisicaByRfc(string rfc)
        {
            httpClient = new HttpClient();
            PersonaFisica result = null;
            var url = configuration.GetSection("EndPoints").GetValue<string>("Api");
            url = url + "/" + rfc;
            using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url))
            {
                using (HttpResponseMessage response = await httpClient.SendAsync(request))
                {
                    using (HttpContent content = response.Content)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var json = await content.ReadAsStringAsync();
                            result = System.Text.Json.JsonSerializer.Deserialize<PersonaFisica>(json);
                        }
                    }
                }
            }
            return result;
        }
        public async Task<string> PutPersonaFisica(int id,PersonaFisica personaFisica)
        {
            httpClient = new HttpClient();
       
            string persona = System.Text.Json.JsonSerializer.Serialize<PersonaFisica>(personaFisica);
            
            var url = configuration.GetSection("EndPoints").GetValue<string>("Api");
            url = url + "/" + id.ToString();
            var requestContent = new StringContent(persona, Encoding.UTF8, "application/json");
            var response = await httpClient.PutAsync(url, requestContent);
            if (response.IsSuccessStatusCode)
            {
                return "Exito";
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();
                return content;
            }
           
        }
        public async Task<string> DeletePersonaFisica(int id)
        {
            httpClient = new HttpClient();

           
            var url = configuration.GetSection("EndPoints").GetValue<string>("Api");
            url = url + "/" + id.ToString();
            var response = await httpClient.DeleteAsync(url);
            if (response.IsSuccessStatusCode)
            {
                return "Exito";
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();
                return content;
            }

        }
        public async Task<string> PostPersonaFisica(PersonaFisica personaFisica)
        {
            httpClient = new HttpClient();

            string persona = System.Text.Json.JsonSerializer.Serialize<PersonaFisica>(personaFisica);
           
            var url = configuration.GetSection("EndPoints").GetValue<string>("Api");
            
            var requestContent = new StringContent(persona, Encoding.UTF8, "application/json");
            var response = await httpClient.PostAsync(url, requestContent);
            if (response.IsSuccessStatusCode)
            {
                return "Exito";
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();
                return content;
            }

        }
    }
}
