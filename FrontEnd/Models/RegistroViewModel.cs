﻿using System.ComponentModel.DataAnnotations;

namespace FrontEnd.Models
{
    public class RegistroViewModel
    {   

        [Required(ErrorMessage ="El campo {0}es requerido")]
        [EmailAddress(ErrorMessage ="El campo debe ser un correo electronico valido")]
        public string Email { get; set; }
        [Required(ErrorMessage = "El campo {0}es requerido")]
        [StringLength(25,MinimumLength =8, ErrorMessage ="la longitud debe ser mayor a 8 caracteres")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
